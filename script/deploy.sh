#!/bin/bash -e
cd ~/sinaurails
./script/puma.sh stop
git status
git pull origin develop
bundle install
RAILS_ENV=production bundle exec rake db:migrate
RAILS_ENV=production bundle exec rake assets:clobber
RAILS_ENV=production bundle exec rake assets:precompile
# wget https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css -P public/assets/.
# s3cmd put --recursive --delete-removed /home/$USER_STAGING/sinaurails/public/assets s3://staging-binar-academy/
chmod +x script/puma.sh
# sudo systemctl restart redis
# sudo systemctl restart sidekiq
./script/puma.sh start
echo "Production aman"

